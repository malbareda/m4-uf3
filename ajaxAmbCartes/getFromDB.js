//aixo es lo mateix que un document ready
$(function() {
    //quan faig click a un button
    $("#b1").click(function () {

        //faig una comanda AJAX
        $.ajax({
            //tipus de metode. GET / POST
            method: "GET",
            //url a la que vols accedir
            url: "getFromDB.php",
            //datatype, tipus de dades que torna
            dataType: 'json',

            //callback
            success: function (data) {
                console.log(data);
                $("#result").text(data);
            },
            error: function (data) {
                alert("F");
                console.log(data);
            }
        });
    });
});