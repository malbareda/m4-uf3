//aixo es lo mateix que un document ready
$(function(){
    //quan faig click a un button
    $("#b2").click(function(){
        
        var valor = $("#inp").val();
        var enviament =
            //faig una comanda AJAX
            $.ajax({
                //tipus de metode. GET / POST
                method:"POST",
                //url a la que vols accedir
                url:"AJAXPOST.php",
                //data es el que envies i hauria de ser un objecte json
                data : {
                    "cosa" : valor
                },

                //callback
                success:function(data) {
                    $("#res").text(data);
                },
                error:function(data){
                    alert("F");
                    console.log(data);
                }
            });
    });
});