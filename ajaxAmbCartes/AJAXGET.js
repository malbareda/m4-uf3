//aixo es lo mateix que un document ready
$(function() {
    //quan faig click a un button
    $("#b1").click(function () {

        //AJAX SERVEIX PER COMUNICARSE AMB ALTRES COSES
        //En la UF3, PHP
        //faig una comanda AJAX
        $.ajax({
            //les comandes AJAX poden ser GET o POST.
            //son bastant diferents entre elles
            //tipus de metode. GET / POST
            method: "GET",
            //url a la que vols accedir
            url: "AJAXGET.php",
            //datatype, tipus de dades que torna
            dataType: 'json',

            //callback
            success: function (data) {
                alert("COOL");
                $("#ikerciego").text(data.Iker);
                console.log(data);
               },
            error: function (data) {
                alert("F");
                console.log(data);
            }
        });
    });
});