//aixo es lo mateix que un document ready
$(function(){
    //quan faig click a un button
    $("#b1").click(function(){

        //faig una comanda AJAX
       $.ajax({
           //tipus de metode. GET / POST
           method:"GET",
           //url a la que vols accedir
           url:"json.php",
           //datatype, tipus de dades que torna
           dataType:'json',

           //callback
           success:function(data) {
                console.log(data);
                console.log(data.color);
                $("#imatge").attr("src",data.img);
           },
           error:function(data){
               alert("F");
               console.log(data);
           }
        });
    });

    $("#b2").click(function(){

        var valor = $("#inp").val();
        console.log("hey"+valor);
        var enviament =
        //faig una comanda AJAX
        $.ajax({
            //tipus de metode. GET / POST
            method:"POST",
            //url a la que vols accedir
            url:"json2.php",
            //datatype, tipus de dades que torna
            data : {
                "cosa" : valor
            },

            //callback
            success:function(data) {
                console.log(data);
            },
            error:function(data){
                alert("F");
                console.log(data);
            }
        });
    });
});