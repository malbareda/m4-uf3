MYSQL
```
CREATE USER 'admin'@'localhost' IDENTIFIED WITH mysql_native_password BY 'super3';
GRANT ALL PRIVILEGES ON *.* TO 'admin'@'localhost' WITH GRANT OPTION;
create database m4Uno;
use m4uno;
create table users(
    id int not null AUTO_INCREMENT,
    nom varchar(50) not null,
    password varchar(20) not null,
    primary key (id)
    );
insert into users ( nom, password) values("Marc","gatete");
insert into users ( nom, password) values ("Pau","castellarnau");
insert into users ( nom, password) values ("Iker","gafas");

create table partida(
    id int not null AUTO_INCREMENT,
    nomPartida varchar(50),
    cartaTaula varchar(1000),
    pilaRobar text,
    torn int,
    numjugs int,
    primary key (id)
);

create table user_partida(
    user_id int not null,
    partida_id int not null,
    ma_jugador text,
    torn_jugador int,
    CONSTRAINT fk_userpartida_user foreign key (user_id) references users(id),
    CONSTRAINT fk_userpartida_partida foreign key (partida_id) references partida(id)
);
```




MARIADB
```
SET old_passwords=0;
CREATE USER admin'@'localhost IDENTIFIED BY 'super3';
RANT ALL PRIVILEGES ON *.* TO 'admin'@'localhost' WITH GRANT OPTION;
create database m4Uno;
use m4uno;
create table users(
    id int not null AUTO_INCREMENT,
    nom varchar(50) not null,
    password varchar(20) not null,
    primary key (id)
    );
insert into users ( nom, password) values("Marc","gatete");
insert into users ( nom, password) values ("Pau","castellarnau");
insert into users ( nom, password) values ("Iker","gafas");

create table partida(
    id int not null AUTO_INCREMENT,
    nomPartida varchar(50),
    cartaTaula varchar(1000),
    pilaRobar text,
    torn int,
    primary key (id)
);

create table user_partida(
    user_id int not null,
    partida_id int not null,
    ma_jugador text,
    torn_jugador int,
    CONSTRAINT fk_userpartida_user foreign key (user_id) references users(id),
    CONSTRAINT fk_userpartida_partida foreign key (partida_id) references partida(id)
);
```





QUALSEVOL DELS DOS, SEMPRE QUE FEU SERVIR PHP 7.4 O POSTERIOR
```
CREATE USER 'admin'@'localhost' IDENTIFIED BY 'super3';
GRANT ALL PRIVILEGES ON *.* TO 'admin'@'localhost' WITH GRANT OPTION;
create database m4Uno;
use m4uno;
create table users(
    id int not null AUTO_INCREMENT,
    nom varchar(50) not null,
    password varchar(20) not null,
    primary key (id)
    );
insert into users ( nom, password) values("Marc","gatete");
insert into users ( nom, password) values ("Pau","castellarnau");
insert into users ( nom, password) values ("Iker","gafas");

create table partida(
    id int not null AUTO_INCREMENT,
    nomPartida varchar(50),
    cartaTaula varchar(1000),
    pilaRobar text,
    torn int,
    primary key (id)
);

create table user_partida(
    user_id int not null,
    partida_id int not null,
    ma_jugador text,
    torn_jugador int,
    CONSTRAINT fk_userpartida_user foreign key (user_id) references users(id),
    CONSTRAINT fk_userpartida_partida foreign key (partida_id) references partida(id)
);
```
