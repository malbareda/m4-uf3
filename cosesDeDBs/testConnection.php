<?php
$servername = "localhost";
$username = "admin";
$password = "super3";



try {
    $conn = new PDO("mysql:host=$servername;dbname=m4uno", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    print_r(json_encode( "Connected successfully"));
} catch(PDOException $e) {
    print_r(json_encode("Connection failed: " . $e->getMessage()));
}
?>