<?php
$servername = "localhost";
$username = "admin";
$password = "super3";

//casteig a int perque ho rebo com una String
$len = (int)$_GET["nchars"];

try {
    $conn = new PDO("mysql:host=$servername;dbname=m4uno", $username, $password);
    $query = $conn->prepare("SELECT * FROM users WHERE LENGTH(nom)>=:length");
    $query->bindParam("length", $len, PDO::PARAM_INT);
    $query->execute();
    //fetchAll torna totes les files que compleixen la query
    $result = $query->fetchAll(PDO::FETCH_ASSOC);
    if($query->rowCount() >= 1) {
        echo(json_encode($result));
    } else{
        echo(json_encode($result));
    }

} catch(PDOException $e) {
    print_r(json_encode("Connection failed: " . $e->getMessage()));
}
?>